/*
 * Author: Adrian J. Paparelli
 * Created: 2017-09-11
 * 
 * Notes: This is the main jobManager method.  The programs will read the input file from args[0]
 *  
 * Assumption: This input file will match the definition from the assignment sheet, other than missing file
 * checking there are no fault tolerances in place for malformed data.  I also assume integer input and that all process
 * ID's will be assigned in consecutive ascending order and begin counting at 1
 */
import java.io.*;
import java.util.Scanner;

import Utils.DependencyManager;
public class JobManager {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		if(args.length > 0)
		{
			File inFile = new File(args[0]);
		
			Scanner scan = new Scanner(inFile);
		
			int V = scan.nextInt();
			int rules = scan.nextInt();
		
			//Create a Dependency Manager Graph with V vertices
			DependencyManager DM = new DependencyManager(V);
		
			//Process N rules defined by the input file
			for(int i = 0; i < rules; i++)
			{
				int job = scan.nextInt();
				int k = scan.nextInt();
			
				//Generate k dependencies for the defined job 
				for(int j = 0; j < k; j++)
				{
					int dependency = scan.nextInt();
					DM.addDependency(job, dependency);
				}
			}
		
			scan.close();
			DM.setOrder();
		}
	}

}
