/*
 * Author: Adrian J. Paparelli
 * Created: 2017-09-11
 * Notes: This is the Dependency Manager class, it is a adjacency list style graph with V+1 Nodes
 * I used V+1 because we start counting at 1 and not 0, I could have also altered the index value, but for
 * now I decided to just add one more index and location 0 should not be used in the code.
 *
 * In order to obtain the job order I perform iterate over each job(vertex) and perform a depth first search to
 * obtain a topological sort.  This sort should result in the order that jobs should be completed.  If there is a
 * dependency cycle the code will throw an exception and exit out of the program.  It's a machete approach to the problem
 * with further time and/or a defined recovery method a more elegant approach can be defined.
 */

package Utils;

import java.util.*;

enum VisitType {NONE, TEMPORARY, PERMANENT}

public class DependencyManager {

	private int V;
	private LinkedList<Integer> adjList[];
	private VisitType visited[];
	private Stack<Integer> processOrder;

	@SuppressWarnings("unchecked")
	public DependencyManager(int V)
	{
		this.V = V;
		//need to add 1 additional array location since we start counting at 1
		adjList = new LinkedList[V+1]; 
		visited = new VisitType[V+1];
		processOrder = new Stack<Integer>();

		for(int i = 1; i < V+1; i++)
		{
			adjList[i] = new LinkedList<Integer>();
			visited[i] = VisitType.NONE;
		}
	}

	//This function is used to groom the adjacency list so that the lower job number is listed for job that do not share a dependency
	//After this is called jobs with a higher index will be considered first and added to the call stack first.
	private void groomOrder()
	{
		for(int i =1; i< V+1; i++)
		{
			Collections.sort(adjList[i], new Comparator<Integer>()
			{
				public int compare(Integer o1, Integer o2)
				{
					if (o1 < o2)
						return 1;
					else if (o1 == o2)
						return 0;
					else
						return -1;
				}
			});
		}
	}

	//Add a job dependency to the adjacency list
	public void addDependency(int job, int dependency)
	{
		adjList[dependency].add(job);
	}


	private void topoSort(int V) throws Exception
	{
		visited[V] = VisitType.TEMPORARY;
		Iterator<Integer> iter = adjList[V].iterator();
		while(iter.hasNext())
		{
			int neighbor = iter.next();
			if(visited[neighbor] == VisitType.TEMPORARY)
				throw new Exception("There are cyclic dependencies!");
			if(visited[neighbor] != VisitType.PERMANENT)
				topoSort(neighbor);
		}
		visited[V] = VisitType.PERMANENT;
		processOrder.push(V);
	}

	public void setOrder() throws Exception
	{
		groomOrder();
		//This is a topological ordering of the graph, I will check
		for(int i = 1; i < V+1; i++)
		{
			if(!(visited[i] == VisitType.PERMANENT))
			{
				topoSort(i);
				printOrder();
			}
		}
		System.out.println("");

	}

	//Output the job order
	private void printOrder()
	{
		String outStr = "";
		while(!processOrder.isEmpty())
		{
			outStr = outStr + " " + Integer.toString(processOrder.pop());

		}
		if(outStr != "")
			System.out.print(outStr);
	}


}
